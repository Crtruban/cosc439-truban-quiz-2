# the compiler: gcc for C porgram, define as g++ for C++
CC = gcc

#compiler flahs:
# -g adds debugging information to the executable file
# -Wall turns on most, but not all. compiler warnings
CFLAGS = -g -Wall -pthread

#the build target executable:
TARGET = q4
all: 
		$(CC) $(CFLAGS) -o question4 $(TARGET).c

clean:
	$(RM) $(TARGET)
